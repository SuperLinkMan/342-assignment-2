
/* ----- INCLUDES ---------------------------------------------------------- */

#include "matrix_ops.h"


/* ---- TYPE DECLARATION ----------------------------------------------------*/
/*
typedef struct Matrix {
  double element[MATRIX_SIZE][MATRIX_SIZE];
} Matrix;
*/


/* ---- FUNCTIONS -----------------------------------------------------------*/

/* Create an identity matrix */
Matrix matrix_identity(void) {

  Matrix result;

  matrix_make(&result,1.0, 0.0, 0.0, 0.0,
                      0.0, 1.0, 0.0, 0.0,
  		      0.0, 0.0, 1.0, 0.0,
         	      0.0, 0.0, 0.0, 1.0);

  return result;
}


/* Replace the passed matrix with the identity matrix*/
void matrix_loadIdentity(Matrix *ident) {
  (*ident) = matrix_identity();
}


/* Construct the matrix from the passed values */
void matrix_make(Matrix *mat,
		double a00, double a01, double a02, double a03,
		double a10, double a11, double a12, double a13,
		double a20, double a21, double a22, double a23,
		double a30, double a31, double a32, double a33) {

  (*mat).element[0][0] = a00;
  (*mat).element[0][1] = a01;
  (*mat).element[0][2] = a02;
  (*mat).element[0][3] = a03;

  (*mat).element[1][0] = a10;
  (*mat).element[1][1] = a11;
  (*mat).element[1][2] = a12;
  (*mat).element[1][3] = a13;

  (*mat).element[2][0] = a20;
  (*mat).element[2][1] = a21;
  (*mat).element[2][2] = a22;
  (*mat).element[2][3] = a23;

  (*mat).element[3][0] = a30;
  (*mat).element[3][1] = a31;
  (*mat).element[3][2] = a32;
  (*mat).element[3][3] = a33;
}


Matrix matrix_multiply(Matrix m1, Matrix m2) {
    int row, col, row_col;
    double sum;
    Matrix result;
    matrix_make(&result,
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
    for (row = 0; row < MATRIX_SIZE; row++) {
        for (col = 0; col < MATRIX_SIZE; col++) {
            sum = 0.0;
            
            for (row_col = 0; row_col < MATRIX_SIZE; row_col++) {
                sum += m1.element[row][row_col] * m2.element[row_col][col];
            }
            
            result.element[row][col] = sum;
        }
    }
    return (result);
}

/*
 * Return the transpose of the provided matrix.
 */
Matrix matrix_transpose(Matrix m1) {
    int row, col;
    Matrix result;
    for (row = 0; row < MATRIX_SIZE; row++) {
        for (col = 0; col < MATRIX_SIZE; col++) {
            result.element[col][row] = m1.element[row][col];
        }
    }
    return (result);
}


/*
 * Add the matrix toAdd to the current sum matrix
 *
 * Sample call:
 *    matrix_add(&result, temp);
 *
 * will update the values in the matrix result.
 */
void matrix_add(Matrix *sum, Matrix toAdd) {

  int row, col;

  for (row = 0; row < MATRIX_SIZE; row++)
    for (col = 0; col < MATRIX_SIZE; col++)
      (*sum).element[row][col] += toAdd.element[row][col];
}


/*
 * Subtract the matrix toSubtract from the current sum matrix
 *
 * Sample call:
 *    matrix_add(&result, temp);
 *
 * will update the values in the matrix result.
 */
void matrix_subtract(Matrix *sum, Matrix toSubtract) {
    int row, col;
    
    for (row = 0; row < MATRIX_SIZE; row++) {
        for (col = 0; col < MATRIX_SIZE; col++) {
            (*sum).element[row][col] -= toSubtract.element[row][col];
        }
    }
}

/*
 * Determine if two matrices are equal (returns 1 for true, 0 for false).
 */
int matrix_equals_matrix(Matrix m1, Matrix m2) {
    int row, col;
    
    for (row = 0; row < MATRIX_SIZE; row++) {
        for (col = 0; col < MATRIX_SIZE; col++) {
            if (m1.element[row][col] != m2.element[row][col]) {
                return 0;
            }
        }
    }
    return 1;
}


/*
 * Transformation of a vector by a matrix.
 */
Vector matrix_vector_multiply(Matrix m1, Vector v2) {
    int row, col;
    double sum;
    Vector result;
    /* Makes things easier to deal with in terms of matrix ops*/
    double vect[4] = {v2.x, v2.y, v2.z, v2.w};
    double vectResult[4] = {0.0, 0.0, 0.0, 0.0};
    
    for (row = 0; row < MATRIX_SIZE; row++) {
        sum = 0.0;
        for (col = 0; col < MATRIX_SIZE; col++) {
            sum += (m1.element[row][col] * vect[col]);
        }
        vectResult[row] = sum;
    }
    
    result.x = vectResult[0];
    result.y = vectResult[1];
    result.z = vectResult[2];
    result.w = vectResult[3];
    
    return (result);
}


/*
 *  Output Matrix - for testing and diagnostics
 */
void matrix_display(Matrix m) {

  int row, col;

  for (row = 0; row < MATRIX_SIZE; row++) {
    printf("    ");
    for (col = 0; col < MATRIX_SIZE; col++)
      printf("%2.4f ", m.element[row][col]);
    printf("\n");
  }
}





