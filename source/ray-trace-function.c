/* the main ray tracing procedure */
RGBColour ray_trace(RayDef ray, int recurse_depth) {
    RGBColour colour, dif_colour, spec_colour, dif_and_spec, ref_colour;
    RayDef reflectedRay;
    double t1, t2; /* The two possible intersection z values */
    double a, b, c;
    double r_dot_e; /* Used by the specular */
    Vector test_vector, v_unit;
    Vector obj_to_light_vector, light_trans_vector;
    Vector p, d, g, n_unit, r_unit, e_unit;
    double closest_t; /* Keeps a track of the closest z value so far */
    ObjectDef closestObject; /* for the closest object hit by the current ray */
    int i;
    
    if (recurse_depth == MAX_RECURSE_DEPTH) {
        /* Stop raytracing and return */
        return colour_black;
    }
    
    v_unit = vector_normalise(ray.direction);
    
    /* Set the colour to background_colour by default and then update it if it
     hits an object. */
    colour = background_colour;
    
    closest_t = 0.0;
    closestObject = object[0];
    
    /* For each object */
    for (i = 0; i < num_objs; i++) {
        
        test_vector = matrix_vector_multiply(object[i].invTransform, ray.start);
        test_vector.y = -test_vector.y; // Account for Open GL's upside down coordinates
        
        // Set up a, b and c
        a = vector_dot_product(v_unit, v_unit);
        b = 2.0 * vector_dot_product(test_vector, v_unit);
        c = vector_dot_product(test_vector, test_vector) - 1.0;
        
        /* The ray hits the sphere. */
        if (b*b > 4*a*c) {
            if (b > 0) {
                t1 = (-b - sqrt(b*b - 4*a*c)) / (2*a);
            } else {
                t1 = (-b + sqrt(b*b - 4*a*c)) / (2*a);
            }
            t2 = (c/(a*t1));
            
            /* Find the closest point out of all the objects and only find the
             colour for that object. Also, only consider points that are in
             front of the camera (+ve t values). */
            if (t1 > 0 && t1 < t2 && (t1 < closest_t || closest_t <= 0.0)) {
                closest_t = t1;
                closestObject = object[i];
            } else if (t2 > 0 && (t2 < closest_t || closest_t <= 0.0)) {
                closest_t = t2;
                closestObject = object[i];
            }
        }
    }
    
    /* After each obect has been considered, find the colour of the closest
     object. */
    if (closest_t > 0.0) {
        test_vector = matrix_vector_multiply(closestObject.invTransform, ray.start);
        test_vector.y = -test_vector.y; // Account for Open GL's upside down coordinates
        
        /* Find the point on the sphere that the ray intersects,
         which is the same as n because it is a unit sphere. */
        p = vector_add(test_vector, vector_scale(v_unit, closest_t - 0.00001));
        n_unit = vector_normalise(p);
        
        ref_colour = colour_black;
        
        if (recurse_depth > 0) {
            printf("Recurse depth: %d\n", recurse_depth);
        }
        
        /* Work out the reflected vector if any of the mirror components are
         not zero. */
        if (closestObject.material.mirror_colour.red > 0.0 ||
            closestObject.material.mirror_colour.green > 0.0 ||
            closestObject.material.mirror_colour.blue > 0.0) {
            
            /* Create a ray using the hit point and newly discovered reflection
             vector. */
            reflectedRay.start = matrix_vector_multiply(closestObject.transform, p);
            reflectedRay.start.y = -reflectedRay.start.y;
            
            reflectedRay.direction = vector_scale(vector_subtract(v_unit, vector_scale(n_unit, 2.0 * vector_dot_product(n_unit, v_unit))), closest_t);
            
            /* Call the raytracing routine and add its result to the overall
             colour of this point. */
            ref_colour = ray_trace(reflectedRay, recurse_depth + 1);
        }
        
        /* Add the lighting effects of each light to the diffuse and specular
         colour. */
        for (i = 0; i < num_lights; i++) {
            
            light_trans_vector = matrix_vector_multiply(closestObject.invTransform, light_source[i].position);
            
            /* To account for Open GL's opposite y direction. */
            g = light_trans_vector;
            g.y = -g.y;
            d = p;
            d.y = -d.y;
            
            /* Only perform lighting calculations for a light if the hit point
             is not in shadow of that light. */
            if (!is_in_shadow(matrix_vector_multiply(closestObject.transform, d), matrix_vector_multiply(closestObject.transform, g), closestObject)) {
                obj_to_light_vector = vector_normalise(vector_subtract(light_trans_vector, n_unit));
                /* Fix for OpenGL's coordinates */
                obj_to_light_vector.y = -obj_to_light_vector.y;
                
                // Get the diffuse colour accounting for texture.
                dif_colour = texture_diffuse(closestObject.material.diffuse_colour, closestObject.material.texture, n_unit);
                
                // Find the proportion of diffuse lighting.
                dif_colour = colour_scale(dif_colour, vector_dot_product(obj_to_light_vector, n_unit));
                
                /* If the difuse colour values are negative, set them to zero. */
                if (dif_colour.red < 0.0) dif_colour.red = 0.0;
                if (dif_colour.green < 0.0) dif_colour.green = 0.0;
                if (dif_colour.blue < 0.0) dif_colour.blue = 0.0;
                
                /* Find the proportion of specular lighting. */
                e_unit = vector_normalise(vector_subtract(test_vector, p));
                r_unit = vector_normalise(vector_subtract(vector_scale(n_unit, 2 * vector_dot_product(obj_to_light_vector, n_unit)), obj_to_light_vector));
                r_dot_e = vector_dot_product(r_unit, e_unit);
                /* If this is smaller than 0.0, then the angle is perpendicular or worse
                 to the normal, which means that there could not physically be any specular
                 reflection. */
                if (r_dot_e > 0.0) {
                    spec_colour = colour_scale(closestObject.material.specular_colour, pow(r_dot_e, closestObject.material.phong));
                } else {
                    spec_colour.red = 0.0;
                    spec_colour.green = 0.0;
                    spec_colour.blue = 0.0;
                }
                
                /* Get the overall colour for the current pixel. */
                dif_and_spec = colour_add(colour_multiply(light_source[i].colour, colour_add(dif_colour, spec_colour)), dif_and_spec);
            }
        }
        
        /* Add the ambient light to the current colour. */
        colour = colour_add(colour_multiply(ambient_light, closestObject.material.ambient_colour), dif_and_spec);
        
        /* Add the reflected light to the current colour. */
        colour = colour_add(colour, colour_multiply(ref_colour, closestObject.material.mirror_colour));
        
        if (recurse_depth > 0) {
            colour_display(colour);
            printf("\n");
        }
        
        /* If the colour is more than 1.0 for any of its components, reset it
         to 1.0. */
        if (colour.red > 1.0) colour.red = 1.0;
        if (colour.green > 1.0) colour.green = 1.0;
        if (colour.blue > 1.0) colour.blue = 1.0;
    }
    
    return (colour);
}