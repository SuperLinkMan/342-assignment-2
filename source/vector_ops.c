/*
 * File: vector_ops.c
 * Author:  Brendan McCane.
 * Updated: Raymond Scurr. (2001).
 *
 * Description: Vector ops. You'll probably want to add more functions here.
 */


/* ---- INCLUDES ------------------------------------------------------------*/
#include "vector_ops.h"



/* ----- TYPE DECLARATIONS ------------------------------------------------- */

/*
 * this a record structure that contains a 4D vector (x, y, z, w).
 *
 * Note - if w == 1 then it represents a point.
 *        if w == 0 then it represents a vector.
 *
 *   typedef struct _Vector {
 *      double x, y, z, w;
 *   } Vector;
 */


/* ----- FUNCTIONS --------------------------------------------------------- */

/* subtract two vectors: return a-b */
Vector vector_subtract(Vector a, Vector b) {

  Vector result;
  
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  result.z = a.z - b.z;
  result.w = a.w - b.w;

  return(result);
}

/* add two vectors: return a+b */
Vector vector_add(Vector a, Vector b) {

  Vector result;
  
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  result.z = a.z + b.z;
  result.w = a.w + b.w;

  return(result);
}

/* return a scaled vector */
Vector vector_scale(Vector a, double s) {
    Vector result;
    result.x = a.x * s;
    result.y = a.y * s;
    result.z = a.z * s;
    return result;
}

/* dot product two vectors: return a.b */
double vector_dot_product(Vector a, Vector b) {
    double result;
    
    result = a.x*b.x + a.y*b.y + a.z*b.z;
    return(result);
}

/* normalise a vector */
Vector vector_normalise(Vector a) {
    double d;
    Vector result;
    
    d = sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
    result.x = a.x/d;
    result.y = a.y/d;
    result.z = a.z/d;
    result.w = 0.0;
    
    return(result);
}


/* display a vector
 *
 *  States if the vector is a  POINT   (w == 1.0)
 *  or if it is a direction    VECTOR  (w == 0.0)
 *
 */
#include <stdio.h>
void   vector_display(Vector v) {

  if (v.w == 0.0) {
    fprintf(stdout, "Vector ");
  } else if (v.w == 1.0){
    fprintf(stdout, "Point  ");
  } else {
    fprintf(stdout, "UNKNOWN ");
  }

  fprintf(stdout, "(%1.3f, %1.3f, %1.3f)", v.x, v.y, v.z);

}
