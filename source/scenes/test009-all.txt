imagesize     640 

background    0.050 0.250 0.400
ambient       0.200 0.200 0.200
lens  35.000

light   -10.000 -5.000 5.000   1.000 1.000 1.000
light   10.000 0.000 5.000   1.000 1.000 1.000

sphere
ambient   0.500 0.500 0.500
diffuse   0.640 0.640 0.640
specular  0.500 0.500 0.500  50.00
mirror    0.000 0.000 0.000
texture   1
stretch	2.000 1.000 1.000
rotate	x 30
translate  -1.550 1.050 -5.000

sphere
ambient   0.500 0.500 0.500
diffuse   0.640 0.640 0.640
specular  0.500 0.500 0.500  50.00
mirror    0.000 0.000 0.000
texture   2
translate  0.550 1.550 -6.000

sphere
ambient   0.500 0.500 0.500
diffuse   0.640 0.640 0.640
specular  0.500 0.500 0.500  50.00
mirror    0.000 0.000 0.000
texture   3
rotate	z 45
translate  -1.050 -1.050 -5.000

sphere
ambient   0.500 0.500 0.500
diffuse   0.640 0.640 0.640
specular  0.500 0.500 0.500  50.00
mirror    0.500 0.500 0.500
texture   4
translate  0.050 0.050 -6.000

endview
