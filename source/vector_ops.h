/*
 * File: vector_ops.h
 *
 * Description: Header file for vector operations.
 */

#ifndef _VECTOR_OPS_H_
#define _VECTOR_OPS_H_


/* ---- TYPE DECLARATIONS -------------------------------------------------- */

/*
 * this a record structure that contains a 4D vector (x, y, z, w).
 *
 * Note - if w == 1 then it represents a point.
 *        if w == 0 then it represents a vector.
 *
 */

typedef struct Vector {
  double x, y, z, w;
} Vector;


/* ---- FUNCTION HEADERS --------------------------------------------------- */

Vector   vector_subtract(Vector a, Vector b);
Vector   vector_add(Vector a, Vector b);
Vector vector_scale(Vector a, double s);
double vector_dot_product(Vector a, Vector b);
Vector vector_normalise(Vector a);

void     vector_display(Vector v);

#endif /* _VECTOR_OPS_H_ */


