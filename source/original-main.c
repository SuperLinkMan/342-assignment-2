/*
 * File: main.c
 * Author: Robert Patchett
 *
 * Description: Contains the main routine plus the ray-tracing function.
 */



/* ----- INCLUDES ---------------------------------------------------------- */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "typedefs.h"
#include "vector_ops.h"
#include "colour_ops.h"
#include "fileio.h"
#include "mygl.h"
#include "ppm.h"


/* ---- Global Variables ----------------------------------------------------*/
/* Must match declarations in typedefs.h
 * Values are loaded in fileio.c
 */

int             MAX_RECURSE_DEPTH;

CameraDef       camera;
int             image_size;
RGBColour       background_colour;
RGBColour       ambient_light;


int             num_lights;
LightSourceDef  light_source[MAX_NUM_LIGHTS];

int             num_objs;
ObjectDef       object[MAX_NUM_OBJS];


/* ----- main prototypes --------------------------------------------------- */
RGBColour ray_trace(RayDef ray, int recurse_depth);
RGBColour texture_diffuse(RGBColour diffuse_colour, int texture, Vector surface_point);

void renderImage(void);
int main (int argc,  char** argv);

/* ----- Functions --------------------------------------------------------- */

/* Geoff's code that may be used for testing Primary rays;
 * it is not part of the final solution. The code appears after main().
 */
bool circle(Vector v);
RGBColour ray_test(RayDef ray);


/* ----- RAY TRACER -------------------------------------------------------- */
/* creates a shadow ray and determines if there is an object obscuring a hit
 point. */
bool is_in_shadow(Vector hitPoint, Vector lightPoint, ObjectDef hitObject) {
    RayDef shadowRay;
    int i;
    double a, b, c, t1, t2;
    bool hit;
    
    hit = false;
    
    /* For each object check if the ray hits it. */
    for (i = 0; i < num_objs; i++) {
        shadowRay.start = hitPoint;
        shadowRay.start = matrix_vector_multiply(object[i].invTransform, shadowRay.start);
        
        shadowRay.direction = vector_subtract(matrix_vector_multiply(object[i].invTransform, lightPoint), shadowRay.start);
        
        /* Set up a, b and c */
        a = vector_dot_product(shadowRay.direction, shadowRay.direction);
        b = 2.0 * vector_dot_product(shadowRay.start, shadowRay.direction);
        c = vector_dot_product(shadowRay.start, shadowRay.start) - 1.0;
        
        /* Check if the ray intersects the sphere. */
        if (b*b > 4*a*c) {
            if (b > 0) {
                t1 = (-b - sqrt(b*b - 4*a*c)) / (2*a);
            } else {
                t1 = (-b + sqrt(b*b - 4*a*c)) / (2*a);
            }
            t2 = (c/(a*t1));
            
            /* Provided neither of the intersect points are too close,
             set hit to true. */
            if ((t1 < 1.0 && t1 > 0.0001) || (t2 < 1.0 && t2 > 0.0001)) {
                hit = true;
            }
        }
    }
    return (hit);
}

/* the main ray tracing procedure */
RGBColour ray_trace(RayDef ray, int recurse_depth) {
    RGBColour colour, dif_colour, spec_colour, dif_and_spec, ref_colour;
    RayDef transformedRay, reflectedRay;
    double t1, t2; /* The two possible intersection z values */
    double a, b, c;
    double r_dot_e; /* Used by the specular */
    Vector obj_to_light_vector, light_trans_vector;
    Vector p, n_unit, r_unit, e_unit;
    double closest_t; /* Keeps a track of the closest z value so far */
    ObjectDef closestObject; /* for the closest object hit by the current ray */
    int i;
    
    if (recurse_depth == MAX_RECURSE_DEPTH) {
        /* Stop raytracing and return */
        return colour_black;
    }
    
    /* Set the colour to background_colour by default and then update it if it
     hits an object. */
    colour = background_colour;
    
    closest_t = 0.0;
    closestObject = object[0];
    
    /* For each object */
    for (i = 0; i < num_objs; i++) {
        
        transformedRay.start = matrix_vector_multiply(object[i].invTransform, ray.start);
        transformedRay.direction = matrix_vector_multiply(object[i].invTransform, ray.direction);
        
        // Set up a, b and c
        a = vector_dot_product(transformedRay.direction, transformedRay.direction);
        b = 2.0 * vector_dot_product(transformedRay.start, transformedRay.direction);
        c = vector_dot_product(transformedRay.start, transformedRay.start) - 1.0;
        
        /* The ray hits the sphere. */
        if (b*b > 4*a*c) {
            if (b > 0) {
                t1 = (-b - sqrt(b*b - 4*a*c)) / (2*a);
            } else {
                t1 = (-b + sqrt(b*b - 4*a*c)) / (2*a);
            }
            t2 = (c/(a*t1));,h
            
            /* Find the closest point out of all the objects and only find the
             colour for that object. Also, only consider points that are in
             front of the camera (+ve t values). */
            if (t1 > 0 && t1 < t2 && (t1 < closest_t || closest_t <= 0.0)) {
                closest_t = t1;
                closestObject = object[i];
            } else if (t2 > 0 && (t2 < closest_t || closest_t <= 0.0)) {
                closest_t = t2;
                closestObject = object[i];
            }
        }
    }
    
    /* After each obect has been considered, find the colour of the closest
     object. */
    if (closest_t > 0.0) {
        transformedRay.start = matrix_vector_multiply(closestObject.invTransform, ray.start);
        
        /* Find the point on the sphere that the ray intersects,
         which is the same as n because it is a unit sphere. */
        p = vector_add(transformedRay.start, vector_scale(ray.direction, closest_t - 0.00001));
        n_unit = vector_normalise(p);
        
        n_unit = matrix_vector_multiply(matrix_transpose(closestObject.invTransform), n_unit);
        
        n_unit = vector_normalise(n_unit);
        
        ref_colour = colour_black;
        
        if (recurse_depth > 0) {
            printf("Recurse depth: %d\n", recurse_depth);
        }
        
        /* Work out the reflected vector if any of the mirror components are
         not zero. */
        if (closestObject.material.mirror_colour.red > 1.0 ||
            closestObject.material.mirror_colour.green > 1.0 ||
            closestObject.material.mirror_colour.blue > 1.0) {
            
            /* Create a ray using the hit point and newly discovered reflection
             vector. */
            reflectedRay.start = matrix_vector_multiply(closestObject.transform, p);
            
            reflectedRay.direction = vector_scale(vector_subtract(ray.direction, vector_scale(n_unit, 2.0 * vector_dot_product(n_unit, ray.direction))), closest_t);
            reflectedRay.direction = matrix_vector_multiply(closestObject.transform, reflectedRay.direction);
            
            /* Call the raytracing routine and add its result to the overall
             colour of this point. */
            ref_colour = ray_trace(reflectedRay, recurse_depth + 1);
        }
        
        /* Add the lighting effects of each light to the diffuse and specular
         colour. */
        for (i = 0; i < num_lights; i++) {
            
            light_trans_vector = matrix_vector_multiply(closestObject.invTransform, light_source[i].position);
            
            /* Only perform lighting calculations for a light if the hit point
             is not in shadow of that light. */
            if (!is_in_shadow(matrix_vector_multiply(closestObject.transform, p), matrix_vector_multiply(closestObject.transform, light_trans_vector), closestObject)) {
                
                obj_to_light_vector = vector_normalise(vector_subtract(light_trans_vector, n_unit));
                
                /* Get the diffuse colour accounting for texture. */
                dif_colour = texture_diffuse(closestObject.material.diffuse_colour, closestObject.material.texture, n_unit);
                
                /* Find the proportion of diffuse lighting. */
                dif_colour = colour_scale(dif_colour, vector_dot_product(obj_to_light_vector, n_unit));
                
                /* If the difuse colour values are negative, set them to zero. */
                if (dif_colour.red < 0.0) dif_colour.red = 0.0;
                if (dif_colour.green < 0.0) dif_colour.green = 0.0;
                if (dif_colour.blue < 0.0) dif_colour.blue = 0.0;
                
                /* Find the proportion of specular lighting. */
                e_unit = vector_normalise(vector_subtract(transformedRay.start, p));
                r_unit = vector_normalise(vector_subtract(vector_scale(n_unit, 2 * vector_dot_product(obj_to_light_vector, n_unit)), obj_to_light_vector));
                r_dot_e = vector_dot_product(r_unit, e_unit);
                /* If this is smaller than 0.0, then the angle is perpendicular or worse
                 to the normal, which means that there could not physically be any specular
                 reflection. */
                if (r_dot_e > 0.0) {
                    spec_colour = colour_scale(closestObject.material.specular_colour, pow(r_dot_e, closestObject.material.phong));
                } else {
                    spec_colour.red = 0.0;
                    spec_colour.green = 0.0;
                    spec_colour.blue = 0.0;
                }
                
                /* Get the overall colour for the current pixel. */
                dif_and_spec = colour_add(colour_multiply(light_source[i].colour, colour_add(dif_colour, spec_colour)), dif_and_spec);
            }
        }
        
        /* Add the ambient light to the current colour. */
        colour = colour_add(colour_multiply(ambient_light, closestObject.material.ambient_colour), dif_and_spec);
        
        /* Add the reflected light to the current colour. */
        colour = colour_add(colour, colour_multiply(ref_colour, closestObject.material.mirror_colour));
        
        if (recurse_depth > 0) {
            colour_display(colour);
            printf("\n");
        }
        
        /* If the colour is more than 1.0 for any of its components, reset it
         to 1.0. */
        if (colour.red > 1.0) colour.red = 1.0;
        if (colour.green > 1.0) colour.green = 1.0;
        if (colour.blue > 1.0) colour.blue = 1.0;
        
    }
    
    return (colour);
}

/* A function to modify the diffuse colour if a material's property
 requests this. Note that when texture==0 diffuse_colour is returned
 unmodified.
 
 Feel free to merge this code into your own ray_trace function: it's
 presented like this so I can give you the texturing code without
 having to try to position them in the ray_trace skeleton code. */
RGBColour texture_diffuse(RGBColour diffuse_colour, int texture, Vector surface_point){
    
    /* used in "random noise" texture */
    static int rseed;
    
    /* Calculate texture coordinates ( ...even if they're not actually used (!) ) */
    double cu = asin(surface_point.x)/M_PI*180;
    double cv = asin(surface_point.y)/M_PI*180;
    double tmp = sqrt(surface_point.x*surface_point.x+surface_point.z*surface_point.z);
    double su = acos(surface_point.x/tmp)/M_PI*180.0;
    double sv = atan(surface_point.y/tmp)/M_PI*180.0;
    
    switch(texture){
        case 0: /* default: don't change diffuse colour at all */
            break;
        case 1: /* checkerboard -- scale original colour */
            /* condition below is Boolean exclusive or */
            if(!((int)(sv+90) % 40 < 20) != !(((int)su+10) % 40 < 20))
                diffuse_colour = colour_scale(diffuse_colour, 0.2);
            break;
        case 2: /* noise -- scale original colour */
            /* rseed function here gives not-really-very-random numbers,
             but its determinism could be useful if, say, you wanted
             to interpolate between successive "random" values... */
            rseed = (rseed * 1103515245 + 12345) & ((1U << 31) - 1);
            diffuse_colour = colour_scale(diffuse_colour, 1.0-((rseed % 10)/30.0));
            break;
        case 3: /* smooth stripes -- scale original colour */
            diffuse_colour = colour_scale(diffuse_colour, 0.8+0.2*sin(2*su/M_PI));
            break;
        case 4: /* rings -- overwrite r+b colour */
            diffuse_colour.red = (0.5+sin(2*cu/M_PI))/2;
            diffuse_colour.blue = (0.5+sin(2*cv/M_PI))/2;
    }
    return diffuse_colour;
}

/* Returns the ray provided but with an updated direction that passes through
 the centre of a given pixel. */
RayDef createRay(RayDef ray, int row, int col) {
    
    /* Add 0.5 to the col/row in order to make the ray pass through the middle
     of each pixel rather than through the corners between pixels. */
    ray.direction.x = (col + 0.5 - image_size/2)/(image_size/camera.view_size);
    ray.direction.y = (row + 0.5 - image_size/2)/(image_size/camera.view_size);
    
    ray.direction.x -= ray.start.x;
    ray.direction.y -= ray.start.y;
    
    /* Alter the direction of the ray slightly so that it passes through the
     centre of the pixel. */
    
    return ray;
}

/*
 *  The main drawing routine.
 *
 *  Use 'alreadyDrawn' to disable redrawing it when part of the picture
 *  is obscured.
 */
void renderImage(void) {
    
    int row, col;
    double step_size;
    RayDef ray;
    RGBColour pixelColour;
    FILE  *picfile;
    
    /* avoid redrawing it if the window is obscured */
    static bool alreadyDrawn = false;
    if (alreadyDrawn) return;
    alreadyDrawn = true;
    /* */
    
    clearScreen();
    
    /* set up the ppm file for writing */
    picfile = fopen("out.ppm", "w");
    initPPM(image_size, image_size, picfile);
    
    /* Calculate the step size */
    step_size = 0;
    
    /* create the start point for the primary ray */
    ray.start.x = 0.0;
    ray.start.y = 0.0;
    ray.start.z = 0.0;
    ray.start.w = 1.0;
    
    /* create the direction of the primary ray */
    ray.direction.x = 0.0;
    ray.direction.y = 0.0;
    ray.direction.z = -camera.lens;
    ray.direction.w = 0.0;
    
    /* create the image pixel by pixel */
    for (row = 0.0; row < image_size; row++) {
        for (col = 0.0; col < image_size; col++) {
            
            /* Update ray direction. */
            ray = createRay(ray, row, col);
            
            /* Find out the colour of this pixel */
            pixelColour = ray_trace(ray, 0); // THIS IS THE CORRECT CODE FOR RAY TRACE
            /*pixelColour = ray_test(ray);*/ // TEMP: USED TO TEST THAT THE CODE WORKS
            
            /* Note that GL draws from bottom, not the top! */
            drawPixel(col, image_size-row-1, pixelColour);
            writePPM(pixelColour, picfile);
        }
    }
    
    /* make sure all of the picture is displayed */
    showScreen();
    
    /* close the ppm file */
    fclose(picfile);
    
    /* finished */
    printf("\nDone\n");
    
}


/* ------------------------------------------------------------------------- */
/* Main */
int main (int argc,  char** argv) {
    
    /* read the scene file */
    fileio_readfile(argv[1]);
    fileio_printscene();
    
    /* set up openGL, and call the render */
    mygl_init(&argc, argv, image_size);
    mygl_make_display_callback(renderImage);
    mygl_mainLoop();
    
    return EXIT_SUCCESS;
}



/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* function that could be useful for testing the early part of the raytracer */
/* ------------------------------------------------------------------------- */
/* circle:
 *
 * will test the ray for intersections with a 'standard' circle
 */

bool circle(Vector v) {
    return (v.z < 0) && (1225 * ((v.x*v.x) + (v.y*v.y)) < 256 * (v.z * v.z));
}

/* ray_test:
 *  Maybe useful for testing the Primary ray, before any sphere intersections
 *  are written
 *
 * For testing purposes,
 *     replace  ray_trace(ray, 0);
 *     with     ray_test(ray);
 *     in the renderImage() double loop.
 *
 * When the camera is in the default position (no transformations) 
 * it should produce a circle
 *
 */
RGBColour ray_test(RayDef ray) {
    
    static RGBColour black = {0.0, 0.0, 0.0};
    static RGBColour white = {1.0, 1.0, 1.0};
    
    if (circle(ray.direction)) {
        return white;
    } else {
        return black;
    }
}
