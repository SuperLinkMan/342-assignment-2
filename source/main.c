/*
 * File: main.c
 * Author: Robert Patchett
 *
 * Description: Contains the main routine plus the ray-tracing function.
 */



/* ----- INCLUDES ---------------------------------------------------------- */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "typedefs.h"
#include "vector_ops.h"
#include "colour_ops.h"
#include "fileio.h"
#include "mygl.h"
#include "ppm.h"


/* ---- Global Variables ----------------------------------------------------*/
/* Must match declarations in typedefs.h
 * Values are loaded in fileio.c
 */

int             MAX_RECURSE_DEPTH;

CameraDef       camera;
int             image_size;
RGBColour       background_colour;
RGBColour       ambient_light;


int             num_lights;
LightSourceDef  light_source[MAX_NUM_LIGHTS];

int             num_objs;
ObjectDef       object[MAX_NUM_OBJS];


/* ----- main prototypes --------------------------------------------------- */
RGBColour ray_trace(RayDef ray, int recurse_depth);
RGBColour texture_diffuse(RGBColour diffuse_colour, int texture, Vector surface_point);

void renderImage(void);
int main (int argc,  char** argv);

/* ----- Functions --------------------------------------------------------- */

/* Geoff's code that may be used for testing Primary rays;
 * it is not part of the final solution. The code appears after main().
 */
bool circle(Vector v);
RGBColour ray_test(RayDef ray);


/* ----- RAY TRACER -------------------------------------------------------- */
/* Return the hit point based on u, v and t. */
Vector hit_point(Vector u, Vector v, double t) {
    Vector p; /* Hit point on sphere. */
    
    p = vector_add(u, vector_scale(v, t));
    
    return p;
}

/* Find and return the smallest positive t value along the ray. */
double distance_of_hit_along_ray(RayDef ray, ObjectDef currentObject) {
    double a, b, c, t1, t2;
    
    a = vector_dot_product(ray.direction, ray.direction);
    b = 2 * vector_dot_product(ray.start, ray.direction);
    c = vector_dot_product(ray.start, ray.start) - 1;
    
    if ((b * b) >= (4 * a * c)) {
        if (b > 0) {
            t1 = (-b - sqrt(b*b - 4*a*c)) / (2*a);
        } else {
            t1 = (-b + sqrt(b*b - 4*a*c)) / (2*a);
        }
        t2 = c / (a*t1);
    } else {
        return -1;
    }
    
    /* Return the smaller value of t that is greater than 0. */
    if (t1 > 0.0 && (t1 < t2 || t2 < 0.0)) {
        return t1;
    } else {
        return t2;
    }
}

/* Creates a shadow ray and determines if there is an object obscuring a hit
 point. */
bool is_in_shadow(Vector hitPoint, Vector lightPoint, ObjectDef hitObject) {
    RayDef shadowRay;
    double t;
    bool hit;
    int i;
    
    hit = false;
    
    for (i = 0; i < num_objs; i++) {
        shadowRay.start = matrix_vector_multiply(object[i].invTransform, hitPoint);
        
        shadowRay.direction = vector_subtract(lightPoint, hitPoint);
        shadowRay.direction = matrix_vector_multiply(object[i].invTransform, shadowRay.direction);
        shadowRay.direction = vector_normalise(shadowRay.direction);
        
        t = distance_of_hit_along_ray(shadowRay, object[i]);
        
        if (t > 0.0 && t < 1.0) {
            hit = true;
        }
    }
    
    return hit;
}

/* Return the diffuse colour of the object at a specific point. */
RGBColour diffuse_colour(ObjectDef obj, Vector normal, Vector normalisedObjToLight) {
    RGBColour colour;
    
    colour = texture_diffuse(obj.material.diffuse_colour, obj.material.texture, normal);
    colour = colour_scale(colour, vector_dot_product(normal, normalisedObjToLight));
    
    /* If the difuse colour values are negative, set them to zero. */
    if (colour.red < 0.0) colour.red = 0.0;
    if (colour.green < 0.0) colour.green = 0.0;
    if (colour.blue < 0.0) colour.blue = 0.0;
    
    return colour;
}

/* the main ray tracing procedure */
RGBColour ray_trace(RayDef ray, int recurse_depth) {
    RGBColour colour, dif_colour, spec_colour, ref_colour;
    RayDef object_space_ray, reflected_ray;
    Vector objectSpaceLightPoint;
    
    double t, closestT;
    ObjectDef closestObject;
    
    Vector hitPoint, normal, normalisedObjToLight, e, reflectedLightVector, transposedNormal;
    double rDotE;
    
    int i;
    
    /* Default colour. */
    colour = colour_black;
    
    /* If the maxRecurse depth is reached, return black and stop any further
     recursion. */
    if (recurse_depth >= MAX_RECURSE_DEPTH) {
        return colour;
    }
    
    /* Set closestT & closestObject. */
    closestT = 0.0;
    closestObject = object[0];
    
    for (i = 0; i < num_objs; i++) {
        /* The ray is in world space, so to make things simpler for ourselves,
         we want it in object space for the particular object we are
         considering. */
        object_space_ray.start = matrix_vector_multiply(object[i].invTransform, ray.start);
        object_space_ray.direction = matrix_vector_multiply(object[i].invTransform, ray.direction);
        
        /* Test for an intersection with the object currently being considered.
         */
        t = distance_of_hit_along_ray(object_space_ray, object[i]);
        if (t > 0.0 && (t < closestT || closestT <= 0.0)) {
            closestT = t;
            closestObject = object[i];
        }
    }
    
    /* If the ray hits no sphere in front of the lense, then return background
     colour. */
    if (closestT <= 0.0) {
        return background_colour;
    }
    
    /* The ray is in world space, so to make things simpler for ourselves,
     we want it in object space for the particular object we are
     considering. */
    object_space_ray.start = matrix_vector_multiply(closestObject.invTransform, ray.start);
    object_space_ray.direction = matrix_vector_multiply(closestObject.invTransform, ray.direction);
    
    /* Offset the hitPoint by a small amount so that it is definitly not inside
     the sphere at all. */
    hitPoint = hit_point(object_space_ray.start, object_space_ray.direction, closestT - 0.000001);
    
    /* Find the normal of the hitPoint. */
    normal = vector_normalise(hitPoint);
    
    /* Find the direction vector from the hitPoint to the camera. */
    e = vector_subtract(object_space_ray.start, hitPoint);
    e = vector_normalise(e);
    
    /* If the object intersect has any reflective component, then create a
     reflected ray and call the ray_trace function recursively. */
    if (closestObject.material.mirror_colour.red > 0.0 ||
        closestObject.material.mirror_colour.green > 0.0 ||
        closestObject.material.mirror_colour.blue > 0.0) {
        
        reflected_ray.start = matrix_vector_multiply(closestObject.transform, hitPoint);
        
        transposedNormal = matrix_vector_multiply(matrix_transpose(closestObject.invTransform), normal);
        transposedNormal = vector_normalise(transposedNormal);
        
        /* Tests break when I declare this variable outside of this loop. */
        Vector eWorld = matrix_vector_multiply(closestObject.transform, e);
        
        reflected_ray.direction = vector_subtract(vector_scale(transposedNormal, (vector_dot_product(eWorld, transposedNormal) * 2)), eWorld);
        
        ref_colour = ray_trace(reflected_ray, recurse_depth + 1);
        
        colour = colour_add(colour, colour_multiply(ref_colour, closestObject.material.mirror_colour));
    }
    
    for (i = 0; i < num_lights; i++) {
        /* Shift the light into object space for the closestObject. */
        objectSpaceLightPoint = matrix_vector_multiply(closestObject.invTransform, light_source[i].position);
        
        if (!is_in_shadow(matrix_vector_multiply(closestObject.transform, hitPoint), light_source[i].position, closestObject)) {
            
            /* Find the normalised vector from the hitPoint to the
             light_source. */
            normalisedObjToLight = vector_normalise(vector_subtract(objectSpaceLightPoint, hitPoint));
            
            /* Determine the diffuse colour for this light source on the
             object. */
            dif_colour = diffuse_colour(closestObject, normal, normalisedObjToLight);
            
            
            /* Find the reflection of the light vector to aid with specular
             reflections. */
            reflectedLightVector = vector_subtract(vector_scale(normal, (2 * vector_dot_product(normalisedObjToLight, normal))), normalisedObjToLight);
            reflectedLightVector = vector_normalise(reflectedLightVector);
            
            /* Find the dot product of r and e to help cancel out unwanted
             specular effects. */
            rDotE = vector_dot_product(reflectedLightVector, e);
            
            /* Determine the specular colour for this light source on the
             object. */
            if (rDotE > 0.0) {
                spec_colour = colour_scale(closestObject.material.specular_colour, pow(rDotE, closestObject.material.phong));
            } else {
                spec_colour = colour_black;
            }
            
            /* Add the colour for this light's diffuse and specular effects
             to the current colour. */
            colour = colour_add(colour, colour_multiply(light_source[i].colour, colour_add(dif_colour, spec_colour)));
        }
    }
    
    colour = colour_add(colour, colour_multiply(ambient_light, closestObject.material.ambient_colour));
    
    /* If the colour is more than 1.0 for any of its components, reset it
     to 1.0. */
    if (colour.red > 1.0) colour.red = 1.0;
    if (colour.green > 1.0) colour.green = 1.0;
    if (colour.blue > 1.0) colour.blue = 1.0;
    
  return (colour);
}

/* A function to modify the diffuse colour if a material's property
   requests this. Note that when texture==0 diffuse_colour is returned
   unmodified.

   Feel free to merge this code into your own ray_trace function: it's
   presented like this so I can give you the texturing code without
   having to try to position them in the ray_trace skeleton code. */
RGBColour texture_diffuse(RGBColour diffuse_colour, int texture, Vector surface_point){
    
    /* used in "random noise" texture */
  static int rseed;

  /* Calculate texture coordinates ( ...even if they're not actually used (!) ) */
  double cu = asin(surface_point.x)/M_PI*180;
  double cv = asin(surface_point.y)/M_PI*180;
  double tmp = sqrt(surface_point.x*surface_point.x+surface_point.z*surface_point.z);
  double su = acos(surface_point.x/tmp)/M_PI*180.0;
  double sv = atan(surface_point.y/tmp)/M_PI*180.0;

  switch(texture){
  case 0: /* default: don't change diffuse colour at all */
    break; 
  case 1: /* checkerboard -- scale original colour */
	  /* condition below is Boolean exclusive or */
    if(!((int)(sv+90) % 40 < 20) != !(((int)su+10) % 40 < 20))
      diffuse_colour = colour_scale(diffuse_colour, 0.2);
    break;
  case 2: /* noise -- scale original colour */
	  /* rseed function here gives not-really-very-random numbers,
	     but its determinism could be useful if, say, you wanted
	     to interpolate between successive "random" values... */
    rseed = (rseed * 1103515245 + 12345) & ((1U << 31) - 1);
    diffuse_colour = colour_scale(diffuse_colour, 1.0-((rseed % 10)/30.0));
    break;
  case 3: /* smooth stripes -- scale original colour */
    diffuse_colour = colour_scale(diffuse_colour, 0.8+0.2*sin(2*su/M_PI));
    break;
  case 4: /* rings -- overwrite r+b colour */
    diffuse_colour.red = (0.5+sin(2*cu/M_PI))/2;
    diffuse_colour.blue = (0.5+sin(2*cv/M_PI))/2;
  }	
  return diffuse_colour;
}

/* Returns the ray provided but with an updated direction that passes through
 the centre of a given pixel. */
RayDef createRay(RayDef ray, int row, int col) {
    
    /* Add 0.5 to the col/row in order to make the ray pass through the middle
     of each pixel rather than through the corners between pixels. Also,
     the y direction is flipped, so that the coordinate system of the objects
     x to the right and y upwards (z out of the screen). */
    ray.direction.x = (col + 0.5 - image_size/2)/(image_size/camera.view_size);
    ray.direction.y = -(row + 0.5 - image_size/2)/(image_size/camera.view_size);
    
    ray.direction.x -= ray.start.x;
    ray.direction.y -= ray.start.y;
    
    return ray;
}

/*
 *  The main drawing routine.
 *
 *  Use 'alreadyDrawn' to disable redrawing it when part of the picture
 *  is obscured.
 */
void renderImage(void) {

  int row, col;
  double step_size;
  RayDef ray;
  RGBColour pixelColour;
  FILE  *picfile;

  /* avoid redrawing it if the window is obscured */
  static bool alreadyDrawn = false;
  if (alreadyDrawn) return;
  alreadyDrawn = true;
  /* */

  clearScreen();

  /* set up the ppm file for writing */
  picfile = fopen("out.ppm", "w");
  initPPM(image_size, image_size, picfile); 

  /* Calculate the step size */
  step_size = 0;
  
  /* create the start point for the primary ray */
  ray.start.x = 0.0;
  ray.start.y = 0.0;
  ray.start.z = 0.0;
  ray.start.w = 1.0;

  /* create the direction of the primary ray */
  ray.direction.x = 0.0;
  ray.direction.y = 0.0;
  ray.direction.z = -camera.lens;
  ray.direction.w = 0.0;

  /* create the image pixel by pixel */
  for (row = 0.0; row < image_size; row++) {
    for (col = 0.0; col < image_size; col++) {
        
        /* Update ray direction. */
        ray = createRay(ray, row, col);
        
        /* Find out the colour of this pixel */
        pixelColour = ray_trace(ray, 0); // THIS IS THE CORRECT CODE FOR RAY TRACE
        /*pixelColour = ray_test(ray);*/ // TEMP: USED TO TEST THAT THE CODE WORKS
        
        /* Note that GL draws from bottom, not the top! */
        drawPixel(col, image_size-row-1, pixelColour);
        writePPM(pixelColour, picfile);
      }
  }
    
  /* make sure all of the picture is displayed */
  showScreen();

  /* close the ppm file */
  fclose(picfile);

  /* finished */
  printf("\nDone\n");

}


/* ------------------------------------------------------------------------- */
/* Main */
int main (int argc,  char** argv) {

  /* read the scene file */
  fileio_readfile(argv[1]);
  fileio_printscene();

  /* set up openGL, and call the render */
  mygl_init(&argc, argv, image_size);
  mygl_make_display_callback(renderImage);
  mygl_mainLoop();

  return EXIT_SUCCESS;
}



/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* function that could be useful for testing the early part of the raytracer */
/* ------------------------------------------------------------------------- */
/* circle:
 *
 * will test the ray for intersections with a 'standard' circle
 */

bool circle(Vector v) {
  return (v.z < 0) && (1225 * ((v.x*v.x) + (v.y*v.y)) < 256 * (v.z * v.z));
}

/* ray_test:
 *  Maybe useful for testing the Primary ray, before any sphere intersections
 *  are written
 *
 * For testing purposes,
 *     replace  ray_trace(ray, 0);
 *     with     ray_test(ray);
 *     in the renderImage() double loop.
 *
 * When the camera is in the default position (no transformations) 
 * it should produce a circle
 *
 */
RGBColour ray_test(RayDef ray) {

  static RGBColour black = {0.0, 0.0, 0.0};
  static RGBColour white = {1.0, 1.0, 1.0};

  if (circle(ray.direction)) {
    return white;
  } else {
    return black;
  }
}
