This project contains a ray tracer and a subfolder with multiple .txt scene files to test its capabilities.
  The ray tracer is capable of raytracing transformed sphere only and includes diffuse, specular and ambient lighting effects, as well as shadows and reflections.

TO RUN:
To run the ray tracer on a particular scene file first cd into the 'source' directory and then run 'make'. Once the program has compiled, type './raytracer <scene_file.txt>.



Contact details:
Robert Patchett
rob.patchett@yahoo.co.nz